import os

import tornado.ioloop
import tornado.web


def capital(s: str) -> str:
    s = s.replace(".", "")
    if len(s) <= 1:
        return s
    if s[0] != " ":
        return s.capitalize()
    return s[0] + s[1:].capitalize()


def strip(st: str) -> str:
    return st.strip()


class TextHandler(tornado.web.RequestHandler):
    def get(self) -> None:
        n = int(self.get_argument("n"))
        s = f"{os.getcwd()}/files/{str(n)}.txt"
        # s = "/home/ladach/devops/rest_easy/files/{}.txt".format(str(n))
        with open(s, "r") as file:
            final = ""
            for line in file:
                line = ".".join(
                    map(
                        capital,
                        " ".join(
                            map(
                                strip,
                                line.replace("!", ".").replace("?", ".").split(" "),
                            )
                        ).split("."),
                    )
                )
                final += line
                if final[len(final) - 1] != ".":
                    final += "."
        self.write(final.strip())


class WordsHandler(tornado.web.RequestHandler):
    def get(self, text: str) -> None:
        text = text.replace("!", ".").replace("?", ".").replace("_", " ").split(" ")
        final_text = ".".join(map(capital, " ".join(map(strip, text)).split(".")))
        if final_text[len(final_text) - 1] != ".":
            final_text += "."
        self.write(final_text.strip())


def make_app() -> tornado.web.Application:
    return tornado.web.Application(
        [(r"/text", TextHandler), (r"/words/(.*)", WordsHandler)]
    )


# async def main() -> None:
#     app = make_app()
#     app.listen(8888)
#     await asyncio.Event().wait()


if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

import json

import requests
import tornado
from tornado.httpclient import AsyncHTTPClient
from tornado.testing import AsyncTestCase, AsyncHTTPTestCase, gen_test
from tornado.web import Application

import src.main
from src.main import make_app


class cerTester(AsyncHTTPTestCase):
    def get_app(self) -> tornado.web.Application:
        app = make_app()
        app.listen(8888)
        return app

    def get_url(self, path):
        """Returns an absolute url for the given path on the test server."""
        return "http://127.0.0.1:8888"

    def test(self) -> None:
        res = self.fetch("http://127.0.0.1:8888/text?n=1")
        assert res.body.decode("utf-8") == "Lala.Sstt gig. Fvdhvb."

    def test2(self) -> None:
        res = self.fetch("http://127.0.0.1:8888/text?n=2")
        assert res.body.decode("utf-8") == "Tet ndh. Fdf.Fff."

    def test3(self) -> None:
        res = self.fetch("http://127.0.0.1:8888/words/dada._faf_gg._k")
        assert res.body.decode("utf-8") == "Dada. Faf gg. K."

    def test4(self) -> None:
        res = self.fetch("http://127.0.0.1:8888/words/text_as_text")
        assert res.body.decode("utf-8") == "Text as text."
